import { stringify } from 'flatted';
/**
 * Summary Logs action and its impact on the state
 */
export class ActionLogger {
    constructor(_logger) {
        this._logger = _logger;
    }
    /**
     * Summary Creates as Redux middleware for logging the actions and its impact on the State
     */
    CreateMiddleware() {
        return store => next => (action) => {
            if (!this.IsLoggingAllowed(action)) {
                return next(action);
            }
            const dispatchedAt = new Date();
            let state = store.getState();
            this.LogActionDispatchStart(state, action);
            let dispatchResult = null;
            try {
                dispatchResult = next(action);
            }
            catch (error) {
                this.LogActionDispatchFailure(action, dispatchedAt, error);
                throw error;
            }
            state = store.getState();
            this.LogActionDispatchComplete(state, action, dispatchedAt);
            return dispatchResult;
        };
    }
    SetLogger(logger) {
        if (this._logger === undefined || this._logger === null)
            this._logger = logger;
        else
            this._logger.SetNextLogger(logger);
    }
    IsLoggingAllowed(action) {
        return action.logEnabled !== undefined
            && action.logEnabled !== null
            && action.logEnabled === true
            && this._logger !== undefined
            && this._logger !== null;
    }
    LogActionDispatchStart(state, action) {
        try {
            var properties = {
                "OldState": stringify(state),
                "ActionName": action.type,
                "DispatchStatus": "Dispatched",
                "DispatchedOn": new Date().toISOString(),
                "Payload": stringify(action.payload)
            };
            this._logger.LogEvent("Fxp.Store.ActionLogger", `${action.type} :: DISPATCHED`, properties);
        }
        catch (error) {
            // Gulp the error
            console.error("ERROR: There was an error while trying to log the Dispatch Complete event");
            console.error(error);
        }
    }
    LogActionDispatchComplete(state, action, dispatchedAt) {
        try {
            let currentTime = new Date();
            const timeTaken = currentTime.getTime() - dispatchedAt.getTime();
            var properties = {
                "NewState": stringify(state),
                "ActionName": action.type,
                "DispatchStatus": "Completed",
                "DispatchedOn": new Date().toISOString(),
                "Payload": stringify(action.payload),
                "TimeTaken": timeTaken.toString()
            };
            this._logger.LogEvent("Fxp.Store.ActionLogger", `${action.type} :: COMPLETED`, properties);
        }
        catch (error) {
            // Gulp the error
            console.error("ERROR: There was an error while trying to log the Dispatch Complete event");
            console.error(error);
        }
    }
    LogActionDispatchFailure(action, dispatchedAt, exception) {
        try {
            let currentTime = new Date();
            const timeTaken = currentTime.getTime() - dispatchedAt.getTime();
            var properties = {
                "ActionName": action.type,
                "DispatchStatus": "Failed",
                "DispatchedOn": new Date().toISOString(),
                "Payload": stringify(action.payload),
                "TimeTaken": timeTaken.toString()
            };
            this._logger.LogEvent("Fxp.Store.ActionLogger", `${action.type} :: FAILED`, properties);
            this._logger.LogException("Fxp.Store.ActionLogger", exception, properties);
            console.error(exception);
        }
        catch (error) {
            // Gulp the error
            console.error("ERROR: There was an error while trying to log the Dispatch Failure event");
            console.error(error);
        }
    }
}
//# sourceMappingURL=action.logger.js.map