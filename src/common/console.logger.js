import { AbstractLogger } from "./abstract.logger";
export class ConsoleLogger extends AbstractLogger {
    constructor(_debugMode = false) {
        super("DEFAULT_CONSOLE_LOGGER");
        this._debugMode = _debugMode;
        this.NextLogger = null;
    }
    processEvent(source, eventName, properties) {
        try {
            if (!this._debugMode)
                return;
            console.log(`EVENT : ${eventName}. (${source})`);
        }
        catch (_a) {
            // DO NOT THROW AN EXCEPTION WHEN LOGGING FAILS
        }
    }
    processException(source, error, properties) {
        try {
            if (!this._debugMode)
                return;
            console.error(error);
        }
        catch (_a) {
            // DO NOT THROW AN EXCEPTION WHEN LOGGING FAILS
        }
    }
}
//# sourceMappingURL=console.logger.js.map