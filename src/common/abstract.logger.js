/**
 * Summary Logs data from application. Follows a Chain of Responsibility pattern where multiple loggers can be chained.
 */
export class AbstractLogger {
    constructor(id) {
        this.LoggerIdentity = id;
    }
    /**
     * Summary Logs an event
     * @param source Location from where the log is sent
     * @param eventName Name of the event that has occurred
     * @param properties Properties (KV pair) associated with the event
     */
    LogEvent(source, eventName, properties) {
        try {
            this.processEvent(source, eventName, properties);
            if (this.NextLogger !== undefined && this.NextLogger !== null) {
                this.NextLogger.LogEvent(source, eventName, properties);
            }
        }
        catch (_a) {
            // DO NOT THROW AN EXCEPTION WHEN LOGGING FAILS
        }
    }
    /**
     * Summary Logs an error in the system
     * @param source Location where the error has occurred
     * @param error Error
     * @param properties Custom properties (KV pair)
     */
    LogException(source, error, properties) {
        try {
            this.processException(source, error, properties);
            if (this.NextLogger !== undefined && this.NextLogger !== null) {
                this.NextLogger.LogException(source, error, properties);
            }
        }
        catch (_a) {
            // DO NOT THROW AN EXCEPTION WHEN LOGGING FAILS
        }
    }
    /**
     * Summary Sets the next logger in the chain. If the next logger is already filled then its chained to the last logger of the chain
     * @param nextLogger Next Logger to be set in the chain
     */
    SetNextLogger(nextLogger) {
        if (nextLogger === undefined || nextLogger === null)
            return;
        if (!this.isLoggerLoopCreated(nextLogger)) {
            if (this.NextLogger === undefined || this.NextLogger === null) {
                this.NextLogger = nextLogger;
            }
            else {
                this.NextLogger.SetNextLogger(nextLogger);
            }
        }
    }
    isLoggerLoopCreated(nextLogger) {
        let tmpLogger = Object.assign({}, nextLogger);
        do {
            if (tmpLogger.LoggerIdentity === this.LoggerIdentity)
                return true;
            tmpLogger = tmpLogger.NextLogger;
        } while (tmpLogger !== null && tmpLogger !== undefined);
        return false;
    }
}
//# sourceMappingURL=abstract.logger.js.map